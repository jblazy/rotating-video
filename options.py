import sys
from utils import fragmentVideo

#help options
def getHelp():
    try:
        printRotation = sys.argv.index('--help')
        print ("--type [infinite:limited] = definir le type de controller\n--print-rotation = affiche le nombre de rotations faites\n--convert-[main:secondary]-video [path] = convertit une video pour l'afficher en tant que video princpale ou secondaire")
        sys.exit(0)
    except ValueError:
        pass

#Get the controller type
def getControllerType():
    try:
        controllerTypeOpt = sys.argv.index('--type')
        controllerType = sys.argv[controllerTypeOpt + 1]
        if controllerType != 'infinite' and controllerType != 'limited' and controllerType != 'auto':
            raise IndexError
        print (controllerType)
        return controllerType
    except IndexError:
        print ("You must define a controller type with the --type flag, it can be infinite or limited")
        sys.exit(0)
    except ValueError:
        print ("You must define a controller type with the --type flag, it can be infinite or limited")
        sys.exit(0)

def setPrintRotation():
    try:
        printRotation = sys.argv.index('--print-rotation')
        return True
    except ValueError:
        return False

#Convert main video to frames in data/main folder
def convertMainVideo():
    try:
        convertVideoOpt = sys.argv.index('--convert-main-video')
        videoPath = sys.argv[convertVideoOpt + 1]
        fragmentVideo(videoPath, 'main')
    except IndexError:
        print ("You must define a video path after the --convert-main-video flag")
    except:
        pass

#Convert secondary video to frames in data/main folder
def convertSecondaryVideo():
    try:
        convertVideoOpt = sys.argv.index('--convert-secondary-video')
        videoPath = sys.argv[convertVideoOpt + 1]
        fragmentVideo(videoPath, 'secondary')
    except IndexError:
        print ("You must define a video path after the --convert-secondary-video flag")
    except:
        pass

def runOptions():
    getHelp()
    options = {}
    options['print_rotation_flag'] = setPrintRotation()
    options['type'] = getControllerType()
    convertMainVideo()
    convertSecondaryVideo()
    return options