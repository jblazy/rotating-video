#DEBIAN
https://gitlab.com/jblazy/rotating-video.git
cd ~/rotating-video
sudo apt-get update -y
sudo apt-get upgrade -y
python3 -m venv .
source ./bin/activate
pip3 install -r requirements.txt
## IMPORTANT
#change code in pyglet --> https://github.com/pyglet/pyglet/pull/96
##

sudo apt-get install -y libqtgui4 libqt4-test libatlas-base-dev libjasper-dev freeglut3-dev vim
# sudo apt-get install -y libhdf5-dev libhdf5-serial-dev libatlas-base-dev libjasper-dev  libqtgui4  libqt4-test libgl1-mesa-dev libgl1-mesa-glx xcompmgr libgl1-mesa-dev libalut0 libalut-dev
#enlever la ligne dans le bashrc
cat rotating-video.d > /etc/init.d/rotating-video
cd /etc/init.d
sudo chmod +x rotating-video
sudo update-rc.d rotating-video defaults
#Apres initialiser le programme avec la video adequate avec le flag --convert-main-video et --convert-secondary-video

echo "stdv_mode=2\ndisable_overscan=0\noverscan_left=0\noverscan_right=-20\noverscan_top=-14\noverscan_bottom=-14\nenable_tvout=1" >> /boot/config.txt

#next step
#Bien configurer la video opencv + rendre efficiente l'install (lancement du programme a l'allumage, etc..)
#S'occuper du flickering
#Bouton pour reset tout l'ordi
#Fonctionnalite pour lire la totalite de la video en un certain nombre de rotations de l'encoder