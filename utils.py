import cv2
import os
import re
import time
import sys

class Reader:
    video_playing = False

    images = []

    def __init__(self, opt):
        self.previous_frame = -1
        self.__current_frame = 0
        
        if opt['type'] != 'auto':
            images_paths = getImages('main')
            self.images = [cv2.imread(image) for image in images_paths]
        self.video = cv2.VideoCapture('video.mp4') 

    @property
    def current_frame(self):
        return self.__current_frame

    @current_frame.setter
    def current_frame(self, current_frame):
        self.previous_frame = self.__current_frame
        self.__current_frame = current_frame

def fragmentVideo(video_path, type):
    #Check if video's path exists
    if not os.path.exists(video_path):
        print("ERROR: video's path does'nt exist")
        return
    cap = cv2.VideoCapture(video_path)
    
    #Create data folder if not exists
    try:
        if not os.path.exists('data'):
            os.makedirs('data')
        if not os.path.exists(f'data/{type}'):
            os.makedirs(f'data/{type}')
    except OSError:
        print ("Error during directories's creation")


    #Remove previous data content
    dataContent = os.listdir(f"data/{type}")
    for file in dataContent:
        os.remove(f"data/{type}/{file}")

    #Fill the data folder with the video's frame
    currentFrame = 0
    while True:
        ret, frame = cap.read()
    
        if ret == False:
            break
        name = f'./data/{type}/{str(currentFrame)}.jpg'
        print (f'Creating ...{name}')
        cv2.imwrite(name, frame, [cv2.IMWRITE_JPEG_QUALITY, 100])
    
        currentFrame += 1
    
    cap.release()
    cv2.destroyAllWindows()

def getImages(type):
    imagesPath = f"data/{type}"

    if not os.path.exists(imagesPath):
        sys.exit(f"ERROR: {imagesPath} folder doesn't exist, you should use --convert-{type}-video flag before")
        return
    images = os.listdir(imagesPath)

    #We sort list according to the frame number
    def getNb(elem):
        m = re.match("[0-9]+", elem)
        return int(m.group())
    images.sort(key=getNb)

    images = [f"{imagesPath}/{img}" for img in images]
    return images