import serial          
import time
import pyglet
# Disable error checking for increased performance
pyglet.options['debug_gl'] = False
import sys
from math import floor

from utils import Reader
from options import runOptions
# importing libraries 
import cv2 
import numpy as np 
import threading

class setInterval :
    def __init__(self,interval,action) :
        self.interval=interval
        self.action=action
        self.stopEvent=threading.Event()
        thread=threading.Thread(target=self.__setInterval)
        thread.start()

    def __setInterval(self) :
        nextTime=time.time()+self.interval
        while not self.stopEvent.wait(nextTime-time.time()) :
            nextTime+=self.interval
            self.action()

    def cancel(self) :
        self.stopEvent.set()


opt = runOptions()

key_pressed = []

#real rotation
real_rotation = 0
#number of the n rotation
rotation = 0

#number of the previous rotation where a frame changed
last_efficient_rotation = 0

#Frame every N frame
n = 1

#n_rest variable will be use in the case of type=limited
#when n will be a float, we will wait every floor(n) to change the current frame
#so we lost the "rest" of the floor.
#To fix this lost, we add the "rest" in n_rest each efficient rotation and when n_rest is higher or equal to 1
#we change the current frame 
n_rest = 0



cv2.namedWindow("window", cv2.WND_PROP_FULLSCREEN)          
cv2.setWindowProperty("window", cv2.WND_PROP_FULLSCREEN, 1)

reader = Reader(opt= opt)


if opt['type'] == 'limited':
    # number of rotations for the whole controller travell 
    # you can find it using --print-rotations options and --type infinite
    all_rotation = 200
    n = all_rotation / len(reader.images)
    print (n)

timer_delay = 20 #en secondes
video_delay = 0.1 #en secondes

def playVideoCV(cap):
    if (cap.isOpened()== False):  
      print("Error opening video  file") 
    
    # Read until video is completed 
    while(cap.isOpened()): 

      # Capture frame-by-frame 
      ret, frame = cap.read()
      if ret == True: 
      
        # Display the resulting frame 
        cv2.imshow('window', frame) 
    
        # Press Q on keyboard to  exit 
        if cv2.waitKey(25) & 0xFF == ord('q'): 
          break
    
      # Break the loop 
      else:  
        break
    


def playVideo():
    print ('playing video...')
    global reader
    reader.video_playing = True

    if opt['type'] == 'auto':
        while True:
            playVideoCV(reader.video)
            reader.video.set(1, 0) #set video to 0 ms
    playVideoCV(reader.video)
    reader.video.set(1, 0)


def is_encoder_rotating():
    data = "is_rotating?\n"
    ser.write(data.encode())
    is_rotating = ser.readline().strip().decode('ascii')
    if is_rotating.find('False') != -1:
        return
    return True

def serialEvent(dt):
    global opt
    if opt['type'] != 'auto' and ser.inWaiting() > 0:
        read = ser.readline().strip().decode('ascii')
        # print (read)
        global reader

        if read.find("increment") != -1 or read.find("decrement") != -1:
            global rotation
            global last_efficient_rotation
            global n_rest
            global real_rotation

            if read.find("increment") != -1:
                real_rotation += 1
            elif read.find("decrement") != -1:
                real_rotation -= 1

            #we slow down virtually the rotation with n_rest //see n_rest comments below
            if n_rest > 1:
                n_rest -= 1
            elif read.find("increment") != -1:
                rotation += 1
            elif read.find("decrement") != -1:
                rotation -= 1
 
            if opt['print_rotation_flag']:
                print ('real rotation:', real_rotation)
                # print ('rotation: ', rotation)

            if rotation % floor(n) == 0 and last_efficient_rotation != rotation:
                n_rest += n - floor(n)
                # print('n: ', n)
                # print('n_rest: ', n_rest)
                # print('current frame: ', reader.current_frame)
                last_efficient_rotation = rotation
                if opt['type'] == 'infinite' and read.find("increment") != -1 and reader.current_frame == len(reader.images) - 1:
                    reader.current_frame = 0
                elif opt['type'] == 'infinite' and read.find("decrement") != -1 and reader.current_frame == 0:
                    reader.current_frame = len(reader.images) - 1
                elif read.find("increment") != -1 and reader.current_frame < len(reader.images) - 1:
                    reader.current_frame += 1
                elif read.find("decrement") != -1 and reader.current_frame != 0:
                    reader.current_frame -= 1
                
                cv2.imshow('window', reader.images[reader.current_frame])
                cv2.waitKey(1)

        if read.find("play") != -1:
            playVideo()


if opt['type'] == 'auto':
    playVideo()
else:
    # ser = serial.Serial('/dev/ttyACM0', 115200)  #Create Serial port object
    ser = serial.Serial('/dev/ttyUSB0', 115200)  #Create Serial port object arduino NANO
    time.sleep(2)

# setInterval(0.1, serialEvent)
pyglet.clock.schedule(serialEvent)
pyglet.app.run()