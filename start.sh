#!/bin/bash
export HOME=/home/pi
export DISPLAY=:0.0
cd $HOME/rotating-video/
source $HOME/rotating-video/bin/activate
export LD_PRELOAD=/usr/lib/arm-linux-gnueabihf/libatomic.so.1.2.0
python $HOME/rotating-video/rotating-video.py --type infinite